// server.js
const express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser'), 
	cors = require('cors'),
	mongoose = require('mongoose'),
    companyRoutes = require('./src/app/expressRoutes/CompanyRoutes'),
    employeeRoutes = require('./src/app/expressRoutes/EmployeeRoutes'),
    addressRoutes = require('./src/app/expressRoutes/AddressRoutes'),
	config = require('./config/mongoDb.js');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_HOST_ADDRESS || config.DB, {
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000
  }).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database: ' + err)}
  );

const app = express();
app.use(bodyParser.json());
app.use(cors());
const port = 4000;

app.use('/Company', companyRoutes);
app.use('/Employee', employeeRoutes);
app.use('/Address', addressRoutes);

const server = app.listen(port, function(){
  console.log('Listening on port ' + port);
});
