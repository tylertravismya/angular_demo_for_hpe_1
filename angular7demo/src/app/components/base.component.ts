import { HttpClient } from '@angular/common/http';
import * as enumTypes from '../models/EnumTypes';

import {CompanyService} from '../services/Company.service';
import {EmployeeService} from '../services/Employee.service';
import {AddressService} from '../services/Address.service';

/** 
	Base class of all Components.  
	For convenience, contains all enums and entity lists 
**/
export class BaseComponent {

    constructor (private http: HttpClient) {}

// enum instances
    CompanyTypes = Object.keys(enumTypes.CompanyType);

// all collection instances
    companys : any;
    employees : any;
    addresss : any;
  
// initialization  
    ngOnInit() {
    }

    initCompanyList() {
        if ( this.companys == null ) {
            new CompanyService(this.http).getCompanys().subscribe(res => {
                this.companys = res;
            });
        }
    }
    
    initEmployeeList() {
        if ( this.employees == null ) {
            new EmployeeService(this.http).getEmployees().subscribe(res => {
                this.employees = res;
            });
        }
    }
    
    initAddressList() {
        if ( this.addresss == null ) {
            new AddressService(this.http).getAddresss().subscribe(res => {
                this.addresss = res;
            });
        }
    }
    
    
// comparison function for select controls  
    compareFn(user1: any, user2: any) {
        return user1 == user2
    }    
}
