import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../../../services/Employee.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Employee/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditEmployeeComponent extends SubBaseComponent implements OnInit {

  employee: any;
  employeeForm: FormGroup;
  title = 'Edit Employee';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: EmployeeService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.employeeForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
   });
  }
  updateEmployee(firstName, lastName) {
    this.route.params.subscribe(params => {
    	this.service.updateEmployee(firstName, lastName, params['id'])
      		.then(success => this.router.navigate(['/indexEmployee']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.employee = this.service.editEmployee(params['id']).subscribe(res => {
        this.employee = res;
      });
    });
    
    super.ngOnInit();
  }
}
