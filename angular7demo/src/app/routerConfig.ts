// routerConfig.ts

import { Routes } from '@angular/router';
import { CreateCompanyComponent } from './components/Company/create/create.component';
import { EditCompanyComponent } from './components/Company/edit/edit.component';
import { IndexCompanyComponent } from './components/Company/index/index.component';
import { CreateEmployeeComponent } from './components/Employee/create/create.component';
import { EditEmployeeComponent } from './components/Employee/edit/edit.component';
import { IndexEmployeeComponent } from './components/Employee/index/index.component';
import { CreateAddressComponent } from './components/Address/create/create.component';
import { EditAddressComponent } from './components/Address/edit/edit.component';
import { IndexAddressComponent } from './components/Address/index/index.component';

export const CompanyRoutes: Routes = [
  { path: 'createCompany',
    component: CreateCompanyComponent
  },
  {
    path: 'editCompany/:id',
    component: EditCompanyComponent
  },
  { path: 'indexCompany',
    component: IndexCompanyComponent
  }
];
export const EmployeeRoutes: Routes = [
  { path: 'createEmployee',
    component: CreateEmployeeComponent
  },
  {
    path: 'editEmployee/:id',
    component: EditEmployeeComponent
  },
  { path: 'indexEmployee',
    component: IndexEmployeeComponent
  }
];
export const AddressRoutes: Routes = [
  { path: 'createAddress',
    component: CreateAddressComponent
  },
  {
    path: 'editAddress/:id',
    component: EditAddressComponent
  },
  { path: 'indexAddress',
    component: IndexAddressComponent
  }
];
