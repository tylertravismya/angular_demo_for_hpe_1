import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule, MatFormFieldModule, MatSelectModule } from '@angular/material';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MatSidenavModule } from '@angular/material/sidenav'


import { IndexCompanyComponent } from './components/Company/index/index.component';
import { CreateCompanyComponent } from './components/Company/create/create.component';
import { EditCompanyComponent } from './components/Company/edit/edit.component';
import { IndexEmployeeComponent } from './components/Employee/index/index.component';
import { CreateEmployeeComponent } from './components/Employee/create/create.component';
import { EditEmployeeComponent } from './components/Employee/edit/edit.component';
import { IndexAddressComponent } from './components/Address/index/index.component';
import { CreateAddressComponent } from './components/Address/create/create.component';
import { EditAddressComponent } from './components/Address/edit/edit.component';

import * as appRoutes from './routerConfig';

import { CompanyService } from './services/Company.service';
import { EmployeeService } from './services/Employee.service';
import { AddressService } from './services/Address.service';

@NgModule({
  declarations: [
    IndexCompanyComponent,
    CreateCompanyComponent,
    EditCompanyComponent,
    IndexEmployeeComponent,
    CreateEmployeeComponent,
    EditEmployeeComponent,
    IndexAddressComponent,
    CreateAddressComponent,
    EditAddressComponent,
    AppComponent
  ],
  imports: [

    BrowserModule, 
    NgbModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
	MatMomentDateModule,
    BrowserAnimationsModule,
	HttpClientModule, 
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule,    
    RouterModule.forRoot(appRoutes.CompanyRoutes), 
    RouterModule.forRoot(appRoutes.EmployeeRoutes), 
    RouterModule.forRoot(appRoutes.AddressRoutes), 
  ],
  providers: [CompanyService,EmployeeService,AddressService],
  bootstrap: [AppComponent]
})
export class AppModule { }
